package btu;

public class Test <GPA, ID>{
    GPA g;
    ID id;

    public void test1_1(Integer x){
        System.out.println(x.getClass().getTypeName());
    }

    public void test1_2(Object time){
        System.out.println(time.getClass().getTypeName());
    }

    public <MYTIME> void test1_3(MYTIME t){
        System.out.println(t.getClass().getTypeName());
    }
}
