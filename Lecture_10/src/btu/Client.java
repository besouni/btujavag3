package btu;

import javax.swing.plaf.synth.SynthTabbedPaneUI;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client extends Thread {

    Socket socket;
    ObjectOutputStream objectOutputStream;
    String message;
    ObjectInputStream objectInputStream;


    @Override
    public void run() {
        try {
            while (true) {
                socket = new Socket(InetAddress.getByName("localhost"), 8080);
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                Scanner scanner = new Scanner(System.in);
                message = scanner.nextLine();
                objectOutputStream.writeObject(message);
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                message = (String) objectInputStream.readObject();
                System.out.println("Server: " + message);
                if(message.equals("bye")){
                    System.exit(0);
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
