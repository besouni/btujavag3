package btu;

import java.sql.*;

public class DB {
    Connection connection;
    String url = "jdbc:mysql://localhost:3306/javabtu3";
    public DB(){
        try {
            connection = DriverManager.getConnection(url, "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void insertUser(String username, String password, String email) {
        String query = "INSERT INTO users (username, password, email) VALUES (?, ?, ?)";
        try {
            PreparedStatement pre =  connection.prepareStatement(query);
            pre.setString(1, username);
            pre.setString(2, password);
            pre.setString(3, email);
            pre.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addTable(String table_name){
        String sql = "CREATE TABLE "+table_name+" " +
                     "(id INTEGER not NULL, " +
                     " first VARCHAR(255), " +
                     " last VARCHAR(255), " +
                     " age INTEGER, " +
                     " PRIMARY KEY ( id ))";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
