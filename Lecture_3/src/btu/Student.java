package btu;

public class Student extends Person {
    double gpi;
    String id;

    public Student() {
        System.out.println("Constructor is running...");
    }

    public Student(double gpi) {
        this.gpi = gpi;
    }

    public String toString() {
        return "Student{" +
                "gpi=" + gpi +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", age=" + age +
                '}';
    }
}

