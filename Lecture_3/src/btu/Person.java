package btu;

public class Person {
    String name = "Ana";
    String lastname;
    int age;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int _age) {
        age = _age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", age=" + age +
                '}';
    }
}
