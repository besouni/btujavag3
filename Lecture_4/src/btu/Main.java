package btu;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int x = 30, y =0;
        int [] m = {3, 4, 5};
        System.out.println("Before exception");

        try{
            System.out.println("Before Try...");
            System.out.println("Try- >"+m[12]);
            System.out.println("Try- >"+(x/y));
            System.out.println("After Try ... ");
        }catch (ArithmeticException e){
            System.out.println("Catch- >"+(x/3));
            System.out.println("Catch- >"+m[0]);
        }finally {
            System.out.println("This is finally block");
        }

        System.out.println("=================");

        try{
            System.out.println("Before Try...");
            System.out.println("Try- >"+m[2]);
            System.out.println("Try- >"+(x/y));
            System.out.println("After Try ... ");
        }catch (ArithmeticException | ArrayIndexOutOfBoundsException e){
            System.out.println("Catch- >"+(x/3));
            System.out.println("Catch- >"+m[0]);
        }


        System.out.println("After exception");

//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }
}
